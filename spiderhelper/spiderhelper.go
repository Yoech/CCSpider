package spiderhelper

import (
	"crypto/tls"
	"errors"
	"fmt"
	"github.com/Yoech/CCSpider/ccutility"
	"github.com/Yoech/CCSpider/config"
	"github.com/gocolly/colly"
	"github.com/gocolly/colly/proxy"
	"github.com/gocolly/colly/queue"
	"github.com/gocolly/redisstorage"
	"github.com/gomodule/redigo/redis"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"
)

// OnRequestCallback .
type OnRequestCallback func(*colly.Collector, *colly.Request)

// OnErrorCallback .
type OnErrorCallback func(*colly.Collector, *colly.Response, error)

// OnResponseCallback .
type OnResponseCallback func(*colly.Collector, *colly.Response)

// OnHTMLCallback .
type OnHTMLCallback func(*colly.Collector, *colly.HTMLElement)

// OnXMLCallback .
type OnXMLCallback func(*colly.Collector, *colly.XMLElement)

// OnScrapedCallback .
type OnScrapedCallback func(*colly.Collector, *colly.Response)

// TagURIInfo .
type TagURIInfo struct {
	Idx uint32
	URI string
}

// TagOnHTMLCallback .
type TagOnHTMLCallback struct {
	HTMLFilter string
	CBHTML     OnHTMLCallback
}

// TagOnXMLCallback .
type TagOnXMLCallback struct {
	XpathQuery string
	CBXML      OnXMLCallback
}

// RunColly .
func RunColly(host string, uriRange []TagURIInfo, cbRequest OnRequestCallback, cbError OnErrorCallback, cbResponse OnResponseCallback, cbHTMLs []TagOnHTMLCallback, cbXMLs []TagOnXMLCallback, cbScraped OnScrapedCallback) (err error) {
	if uriRange == nil || len(uriRange) == 0 {
		return errors.New("uriRange can't be empty")
	}

	var s *redisstorage.Storage
	var q *queue.Queue
	cache := config.StoragePool.Get()
	defer config.StoragePool.Put(cache)
	if cache != nil {
		x := cache.(*config.TagCacheInfo)
		s = x.S
		q = x.Q
	} else {
		q, _ = queue.New(1, &queue.InMemoryQueueStorage{MaxSize: 10000})
	}

	//colly.AllowedDomains(uri),
	//colly.MaxDepth(1),
	//colly.Debugger(&debug.LogDebugger{}),
	c := colly.NewCollector()

	WithTransport(c, 10, 1, 5)

	err = SetProxyFunc(c)
	if err != nil {
		log.Printf("err[%v]", err)
		return err
	}

	c.AllowURLRevisit = true

	// 简单的防反设置
	_ = c.Limit(&colly.LimitRule{
		// 指定的域名
		DomainGlob: "*",
		// 并发请求数
		Parallelism: 1,
		// 发起请求时的等待时间
		Delay: 2 * time.Second,
		// 额外的随机延迟
		RandomDelay: 2 * time.Second,
	})

	OnRequest(c, host, "", cbRequest)
	err = OnError(c, cbError)
	if err != nil {
		log.Printf("OnError.err[%v]", err)
		return err
	}
	OnResponse(c, cbResponse)
	OnHTML(c, cbHTMLs)
	OnXML(c, cbXMLs)
	OnScraped(c, cbScraped)

	if s != nil {
		err = c.SetStorage(s)
		if err != nil {
			log.Printf("SetStorage.err[%v]", err)
			return err
		}
	}

	if s != nil {
		for _, v := range uriRange {
			//err = collyQueue.AddURL(v.Uri)

			u, err := url.Parse(v.URI)
			if err == nil {
				r := &colly.Request{
					URL:     u,
					Method:  "GET",
				}

				// *map[string][]string
				r.Headers = &http.Header{}

				// reset per request's headers
				OnHeaderSet(r, host, v.URI)

				// put our ext-data here~
				// Cool.Cat
				r.Ctx = colly.NewContext()
				r.Ctx.Put("Idx", v.Idx)
				err = q.AddRequest(r)
				if err != nil {
					return nil
				}
			}

		}
		q.Run(c)
	} else {
		c.Async = true
		for _, v := range uriRange {
			err = c.Visit(v.URI)
		}
		c.Wait()
	}

	return err
}

// WithTransport .
func WithTransport(c *colly.Collector, maxIdleConns int, maxIdleConnsPerHost int, idleConnTimeout int) {
	c.WithTransport(&http.Transport{
		DisableKeepAlives:   false,
		TLSClientConfig:     &tls.Config{InsecureSkipVerify: true},        // 不校验服务端证书
		MaxIdleConns:        maxIdleConns,                                 //所有Host最大连接数
		MaxIdleConnsPerHost: maxIdleConnsPerHost,                          // 每个Host最大连接数
		IdleConnTimeout:     time.Duration(idleConnTimeout) * time.Second, // 连接空闲超时
	})
}

// SetProxyFunc .
func SetProxyFunc(c *colly.Collector) (err error) {
	var rp colly.ProxyFunc
	proxyIP := RandomProxy()
	if proxyIP != "" {
		rp, err = proxy.RoundRobinProxySwitcher(proxyIP)
		if err != nil {
			return fmt.Errorf("randomProxy.proxyIP[%v].err[%v]", proxyIP, err)
		}
		c.SetProxyFunc(rp)
		//log.Printf("randomProxy.proxyIP[%v]", proxyIP)
	}
	return err
}

func OnHeaderSet(r *colly.Request, host string, url string) {
	r.Headers.Set("authority", host)
	r.Headers.Set("method", "GET")
	url = strings.ReplaceAll(url, host, "")
	if strings.LastIndex(url, "/") < len(url) {
		url += "/"
	}
	r.Headers.Set("path", url)
	r.Headers.Set("scheme", "https")

	r.Headers.Set("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
	//r.Headers.Set("Accept-Encoding", "gzip, deflate, br")
	r.Headers.Set("Accept-Language", "en,zh-CN;q=0.9,zh;q=0.8,zh-TW;q=0.7")

	r.Headers.Set("cache-control", "no-cache")
	r.Headers.Set("cookie", "Hm_lvt_dbc355aef238b6c32b43eacbbf161c3c=1578329760,1578431951; Hm_lpvt_dbc355aef238b6c32b43eacbbf161c3c=1578432504")
	r.Headers.Set("pragma", "no-cache")

	r.Headers.Set("Referer", host)

	r.Headers.Set("sec-fetch-mode", "navigate")
	r.Headers.Set("sec-fetch-site", "cross-site")
	r.Headers.Set("sec-fetch-user", "?1")
	r.Headers.Set("Upgrade-Insecure-Requests", "1")

	r.Headers.Set("User-Agent", RandomAgent())
}

// OnRequest .
func OnRequest(c *colly.Collector, host string, url string, cbRequest OnRequestCallback) {
	c.OnRequest(func(r *colly.Request) {

		OnHeaderSet(r, host, url)

		if cbRequest != nil {
			cbRequest(c, r)
		}
	})
}

// OnError .
func OnError(c *colly.Collector, cbError OnErrorCallback) (err error) {
	c.OnError(func(r *colly.Response, e error) {
		err = e
		if cbError != nil {
			cbError(c, r, e)
		}
	})
	return err
}

// OnResponse .
func OnResponse(c *colly.Collector, cbResponse OnResponseCallback) {
	c.OnResponse(func(r *colly.Response) {
		if cbResponse != nil {
			cbResponse(c, r)
		}
	})
}

// OnHTML .
func OnHTML(c *colly.Collector, cbHTMLs []TagOnHTMLCallback) {
	if cbHTMLs == nil {
		return
	}
	var wg = &sync.WaitGroup{}
	for _, v := range cbHTMLs {
		if v.HTMLFilter != "" && len(v.HTMLFilter) > 0 {
			wg.Add(1)
			go func(WG *sync.WaitGroup, c *colly.Collector, a TagOnHTMLCallback) {
				defer WG.Done()
				c.OnHTML(a.HTMLFilter,
					func(r *colly.HTMLElement) {
						a.CBHTML(c, r)
					},
				)
			}(wg, c, v)
		}
	}
	wg.Wait()
}

// OnXML .
func OnXML(c *colly.Collector, cbXMLs []TagOnXMLCallback) {
	if cbXMLs == nil {
		return
	}
	var wg = &sync.WaitGroup{}
	for _, v := range cbXMLs {
		if v.XpathQuery != "" && len(v.XpathQuery) > 0 {
			wg.Add(1)
			go func(WG *sync.WaitGroup, c *colly.Collector, a TagOnXMLCallback) {
				defer WG.Done()
				c.OnXML(v.XpathQuery,
					func(r *colly.XMLElement) {
						a.CBXML(c, r)
					},
				)
			}(wg, c, v)
		}
	}
	wg.Wait()
}

// OnScraped .
func OnScraped(c *colly.Collector, cbScraped OnScrapedCallback) {
	c.OnScraped(func(r *colly.Response) {
		if cbScraped != nil {
			cbScraped(c, r)
		}
	})
}

// RandomAgent .
func RandomAgent() string {
	agent := [...]string{
		"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36",
		"Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.1",
		"Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; en) Presto/2.8.131 Version/11.12",
		"Opera/9.80 (Windows NT 6.1; U; en) Presto/2.8.131 Version/11.12",
		"Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2; 360SE)",
		"Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.2",
		"Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2; The World)",
		"User-Agent,Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.51",
		"User-Agent, Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Maxthon 2.1)",
		"User-Agent,Mozilla/5.0 (Windows; U; Windows NT 6.1; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.51",
	}

	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	len := len(agent)
	return agent[r.Intn(len)]
}

// RandomProxy .
func RandomProxy() string {
	var val string
	var err error
	var key []interface{}
	key, err = redis.Values(config.RedisPool.Exec("ZREVRANGEBYSCORE", config.GlobalURIProxyZset, config.GlobalProxyMaxScore, config.GlobalProxyMaxScore))
	if err != nil || key == nil {
		return ""
	}

	maxNum := len(key)
	if maxNum == 0 {
		return ""
	}

	rnd := ccutility.RndNum(0, int64(maxNum))

	val, err = redis.String(key[rnd], err)
	if err != nil {
		log.Printf("RandomProxy.key[%v].err[%v]", key[rnd], err)
		return ""
	}

	val = strings.Replace(strings.ToLower(val), "https://", "http://", -1)

	return val
}
