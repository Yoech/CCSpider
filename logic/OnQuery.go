package logic

import (
	"github.com/gocolly/colly"
)

// TagQueryInfo .
type TagQueryInfo struct {
	Rsp []byte
}

// AllocQuery .
func AllocQuery() *TagQueryInfo {
	p := &TagQueryInfo{}
	return p
}

// OnRequestCallback .
func (p *TagQueryInfo) OnRequestCallback(c *colly.Collector, r *colly.Request) {

}

// OnErrorCallback .
func (p *TagQueryInfo) OnErrorCallback(c *colly.Collector, r *colly.Response, e error) {

}

// OnResponseCallback .
func (p *TagQueryInfo) OnResponseCallback(c *colly.Collector, r *colly.Response) {
	p.Rsp = r.Body
}

// OnHTMLCallback .
func (p *TagQueryInfo) OnHTMLCallback(c *colly.Collector, r *colly.HTMLElement) {

}

// OnXMLCallback .
func (p *TagQueryInfo) OnXMLCallback(c *colly.Collector,
	r *colly.XMLElement) {

}

// OnScrapedCallback .
func (p *TagQueryInfo) OnScrapedCallback(c *colly.Collector,
	r *colly.Response) {
}
