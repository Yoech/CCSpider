package logic

import (
	"github.com/Yoech/CCSpider/ccutility"
	"github.com/Yoech/CCSpider/config"
	"github.com/Yoech/CCSpider/spiderhelper"
	"github.com/gocolly/colly"
	"strconv"
	"strings"
	"sync"
)

// TagRawURI .
type TagRawURI struct {
	CountSucc     int64
	CountFail     int64
	pageURI       string
	RawURIFilter  string
	cateFilter    string
	cateURIFilter string
	tagsFilter    string
	tagsURIFilter string
	RawURI        map[string][]string
	RawCate       map[string][]string
	RawTags       map[string][]string
	PageError     []spiderhelper.TagURIInfo
	lock          []*sync.RWMutex
}

// AllocRawURI .
func AllocRawURI(pageURI string, cateFilter string, cateURIFilter string, tagsFilter string, tagsURIFilter string, RawURIFilter string) *TagRawURI {
	p := &TagRawURI{0, 0, pageURI, RawURIFilter, cateFilter, cateURIFilter, tagsFilter, tagsURIFilter, make(map[string][]string), make(map[string][]string, 0), make(map[string][]string, 0), make([]spiderhelper.TagURIInfo, 0), make([]*sync.RWMutex, 2)}
	for k := range p.lock {
		p.lock[k] = new(sync.RWMutex)
	}
	return p
}

// OnRequestCallback .
func (p *TagRawURI) OnRequestCallback(c *colly.Collector, r *colly.Request) {

}

// OnErrorCallback .
func (p *TagRawURI) OnErrorCallback(c *colly.Collector, r *colly.Response, e error) {
	// save error except non-existent pages
	if r.StatusCode != 404 {
		errURI := ccutility.RemoveLastSlash(r.Request.URL.Scheme + "://" + r.Request.URL.Host + r.Request.URL.Path)

		var idx float64
		IdxStr := r.Ctx.GetAny("Idx")
		if IdxStr != nil {
			idx = IdxStr.(float64)
		}

		p.lock[0].Lock()
		p.CountFail++
		p.PageError = append(p.PageError, spiderhelper.TagURIInfo{Idx: uint32(idx), URI: errURI})
		p.lock[0].Unlock()

		config.RedisPool.Exec("HMSET", config.GlobalErrorRawURI, errURI, strconv.Itoa(r.StatusCode)+"|"+e.Error())
	}
	//r.Request.Retry()
}

// OnResponseCallback .
func (p *TagRawURI) OnResponseCallback(c *colly.Collector, r *colly.Response) {

}

// OnHTMLCallbackForRawURI .
func (p *TagRawURI) OnHTMLCallbackForRawURI(c *colly.Collector, r *colly.HTMLElement) {
	pageURI := ccutility.RemoveLastSlash(r.Request.URL.Scheme + "://" + r.Request.URL.Host + r.Request.URL.Path)

	Link := r.Request.AbsoluteURL(r.Attr("src"))

	p.lock[1].Lock()
	p.CountSucc++
	p.RawURI[pageURI] = append(p.RawURI[pageURI], Link)
	p.lock[1].Unlock()
}

// OnHTMLCallbackForRawCate .
func (p *TagRawURI) OnHTMLCallbackForRawCate(c *colly.Collector, r *colly.HTMLElement) {
	pageURI := ccutility.RemoveLastSlash(r.Request.URL.Scheme + "://" + r.Request.URL.Host + r.Request.URL.Path)

	p.lock[1].Lock()
	p.RawCate[pageURI] = append(p.RawCate[pageURI], r.Text)
	p.RawCate[pageURI] = append(p.RawCate[pageURI], strings.Replace(r.Attr("href"), p.cateURIFilter, "", -1))
	p.lock[1].Unlock()
}

// OnHTMLCallbackForRawTags .
func (p *TagRawURI) OnHTMLCallbackForRawTags(c *colly.Collector, r *colly.HTMLElement) {
	pageURI := ccutility.RemoveLastSlash(r.Request.URL.Scheme + "://" + r.Request.URL.Host + r.Request.URL.Path)

	p.lock[1].Lock()
	p.RawTags[pageURI] = append(p.RawTags[pageURI], r.Text)
	p.RawTags[pageURI] = append(p.RawTags[pageURI], strings.Replace(r.Attr("href"), p.tagsURIFilter, "", -1))
	p.lock[1].Unlock()
}

// OnXMLCallback .
func (p *TagRawURI) OnXMLCallback(c *colly.Collector, r *colly.XMLElement) {

}

// OnScrapedCallback .
func (p *TagRawURI) OnScrapedCallback(c *colly.Collector, r *colly.Response) {
	pageURI := ccutility.RemoveLastSlash(r.Request.URL.Scheme + "://" + r.Request.URL.Host + r.Request.URL.Path)

	config.RedisPool.Exec("HDEL", config.GlobalErrorRawURI, pageURI)
}
