package logic

import (
	"bytes"
	"github.com/PuerkitoBio/goquery"
	"github.com/Yoech/CCSpider/config"
	"github.com/gocolly/colly"
	"log"
	"strconv"
)

// TagPageTotal .
type TagPageTotal struct {
	indexURI    string
	hostURI     string
	PageTotal   int
	navLink     string
	pageNumber  string
	bSave2Redis bool
}

// AllocPageTotal .
func AllocPageTotal(hostURI string, indexURI string, navLink string, pageNumber string, bSave2Redis bool) *TagPageTotal {
	p := &TagPageTotal{indexURI, hostURI, 0, navLink, pageNumber, bSave2Redis}
	return p
}

// OnRequestCallback .
func (p *TagPageTotal) OnRequestCallback(c *colly.Collector, r *colly.Request) {

}

// OnErrorCallback .
func (p *TagPageTotal) OnErrorCallback(c *colly.Collector, r *colly.Response, e error) {
	log.Printf("OnErrorCallback.StatusCode[%v]", r.StatusCode)
}

// OnResponseCallback .
func (p *TagPageTotal) OnResponseCallback(c *colly.Collector, r *colly.Response) {
	dom, err := goquery.NewDocumentFromReader(bytes.NewReader(r.Body))
	if err != nil {
		log.Printf("NewDocumentFromReader err[%v]", err.Error())
		return
	}

	navLinks := dom.Find(p.navLink)
	if navLinks.Nodes == nil {
		log.Printf("DOM can't find navLink with [%v]", p.navLink)
		return
	}
	var pageCurrent int
	navLinks.Each(func(i int, sel *goquery.Selection) {
		pageNumbers := sel.Find(p.pageNumber)
		if pageNumbers.Nodes == nil {
			log.Printf("DOM can't find pageNumber with [%v]", p.pageNumber)
			return
		}
		pageNumbers.EachWithBreak(func(n int, sel2 *goquery.Selection) bool {
			pageText := sel2.Text()
			pageCurrent, err = strconv.Atoi(pageText)
			if err != nil {
				log.Printf("DOM can't convert pageText [%v] to integer", pageText)
				return true
			}
			if pageCurrent > p.PageTotal {
				p.PageTotal = pageCurrent
			}
			return true
		})
		log.Printf("[%v].pageTotal[%v]", p.indexURI, p.PageTotal)

		if p.bSave2Redis {
			config.RedisPool.Exec("HMSET", p.indexURI, "pageTotal", strconv.Itoa(p.PageTotal))
		}
	})
}

// OnHTMLCallback .
func (p *TagPageTotal) OnHTMLCallback(c *colly.Collector, r *colly.HTMLElement) {

}

// OnXMLCallback .
func (p *TagPageTotal) OnXMLCallback(c *colly.Collector,
	r *colly.XMLElement) {

}

// OnScrapedCallback .
func (p *TagPageTotal) OnScrapedCallback(c *colly.Collector,
	r *colly.Response) {
}
