package logic

import (
	"github.com/Yoech/CCSpider/ccutility"
	"github.com/Yoech/CCSpider/config"
	"github.com/Yoech/CCSpider/spiderhelper"
	"github.com/gocolly/colly"
	"log"
	"regexp"
	"strconv"
	"sync"
)

// TagPageList .
type TagPageList struct {
	hostURI          string
	pageURI          string
	pageTotal        int
	bConcurrent      bool
	CountSucc        int64
	CountFail        int64
	PageList         string
	PageListAttr     string
	IndexImage       string
	IndexImageAttr   string
	IndexImageRegexp []string
	PageDetails      map[string][]string
	IndexDetails     map[string][]string
	PageError        []spiderhelper.TagURIInfo
	lock             []*sync.RWMutex
}

// AllocPageList .
func AllocPageList(hostURI string, pageURI string, pageTotal int, pageList string, pageListAttr string, indexImage string, indexImageAttr string, indexImageRegexp []string, bConcurrent bool) *TagPageList {
	p := &TagPageList{hostURI, pageURI, pageTotal, bConcurrent, 0, 0, pageList, pageListAttr, indexImage, indexImageAttr, indexImageRegexp, make(map[string][]string), make(map[string][]string, 0), make([]spiderhelper.TagURIInfo, 0), make([]*sync.RWMutex, 2)}
	for k := range p.lock {
		p.lock[k] = new(sync.RWMutex)
	}
	return p
}

// OnRequestCallback .
func (p *TagPageList) OnRequestCallback(c *colly.Collector, r *colly.Request) {

}

// OnErrorCallback .
func (p *TagPageList) OnErrorCallback(c *colly.Collector, r *colly.Response, e error) {
	// save error except non-existent pages
	if r.StatusCode != 404 {
		errURI := ccutility.RemoveLastSlash(r.Request.URL.Scheme + "://" + r.Request.URL.Host + r.Request.URL.Path)

		var idx float64
		IdxStr := r.Ctx.GetAny("Idx")
		if IdxStr != nil {
			idx = IdxStr.(float64)
		}

		p.lock[0].Lock()
		p.CountFail++
		p.PageError = append(p.PageError, spiderhelper.TagURIInfo{Idx: uint32(idx), URI: errURI})
		p.lock[0].Unlock()

		config.RedisPool.Exec("HMSET", config.GlobalErrorPage, errURI, strconv.Itoa(r.StatusCode)+"|"+e.Error())
	}
	//r.Request.Retry()
}

// OnResponseCallback .
func (p *TagPageList) OnResponseCallback(c *colly.Collector, r *colly.Response) {

}

// OnHTMLCallbackForPageList .
func (p *TagPageList) OnHTMLCallbackForPageList(c *colly.Collector, r *colly.HTMLElement) {
	pageURI := ccutility.RemoveLastSlash(r.Request.URL.Scheme + "://" + r.Request.URL.Host + r.Request.URL.Path)

	p.lock[1].Lock()
	defer p.lock[1].Unlock()

	Title := r.Text
	Link := r.Request.AbsoluteURL(r.Attr(p.PageListAttr))

	p.PageDetails[pageURI] = append(p.PageDetails[pageURI], Title, Link)
	p.CountSucc++
}

// OnHTMLCallbackForIndexImage .
func (p *TagPageList) OnHTMLCallbackForIndexImage(c *colly.Collector, r *colly.HTMLElement) {
	pageURI := ccutility.RemoveLastSlash(r.Request.URL.Scheme + "://" + r.Request.URL.Host + r.Request.URL.Path)

	p.lock[1].Lock()
	defer p.lock[1].Unlock()

	IndexImageURI := r.Request.AbsoluteURL(r.Attr(p.IndexImageAttr))

	if len(IndexImageURI) == 0 {
		log.Printf("pageURI[%v].IndexImageURI.nil", pageURI)
		return
	}

	var pageIdx = ""

	for i := range p.IndexImageRegexp {
		reg, err := regexp.Compile(p.IndexImageRegexp[i])
		if err != nil {
			log.Printf("pageURI[%v].regexp[%v].Compile[%v].err[%v]", pageURI, p.IndexImageRegexp, IndexImageURI, err)
			continue
		}
		b := reg.FindStringSubmatch(IndexImageURI)
		if len(b) == 0 {
			//log.Printf("pageURI[%v].regexp[%v].FindString[%v].nil", pageURI, p.IndexImageRegexp, IndexImageURI)
			continue
		}

		// /thumbs/([\d]+)/([\d]+)/([\d]+)_([\w\-]+)_[\d]+([\.\w]+)$
		// https://i5.meizitu.net/thumbs/2019/08/200865_30a04_236.jpg
		// =>
		// /thumbs/2019/08/200865_30a04_236.jpg
		// 2019
		// 08
		// 20085
		// 30a04
		// .jpg

		//// e.g: /2019/08/28c47.jpg
		//IndexImageURI = "/" + b[1] + "/" + b[2] + "/" + b[4] + b[5]

		//// e.g: /thumbs/2019/09/203873_20b03_236.jpg
		//IndexImageURI = b[0]

		pageIdx = "/" + b[3]
	}

	p.IndexDetails[pageURI] = append(p.IndexDetails[pageURI], p.hostURI+pageIdx, IndexImageURI)
	p.CountSucc++
}

// OnXMLCallback .
func (p *TagPageList) OnXMLCallback(c *colly.Collector, r *colly.XMLElement) {

}

// OnScrapedCallback .
func (p *TagPageList) OnScrapedCallback(c *colly.Collector, r *colly.Response) {
	pageURI := ccutility.RemoveLastSlash(r.Request.URL.Scheme + "://" + r.Request.URL.Host + r.Request.URL.Path)

	config.RedisPool.Exec("HDEL", config.GlobalErrorPage, pageURI)

	//p.lock[1].RLock()
	//if p.PageDetails[pageURI] != nil {
	//	pageURIKey := ccutility.ReplaceSlash(pageURI)
	//	config.RedisPool.Exec("HMSET", redis.Args{}.Add(pageURIKey).AddFlat(p.PageDetails[pageURI])...)
	//}
	//p.lock[1].RUnlock()
}
