package logic

import (
	"github.com/Yoech/CCSpider/ccutility"
	"github.com/Yoech/CCSpider/config"
	"github.com/Yoech/CCSpider/spiderhelper"
	"github.com/gocolly/colly"
	"strconv"
	"sync"
)

// TagMeta .
type TagMeta struct {
	Length int
	Data   []byte
}

// TagRawData .
type TagRawData struct {
	CountSucc int64
	CountFail int64
	pageURI   string
	RawMeta   map[string][]TagMeta
	PageError []spiderhelper.TagURIInfo
	lock      []*sync.RWMutex
}

// AllocRawData .
func AllocRawData(pageURI string) *TagRawData {
	p := &TagRawData{0, 0, pageURI, make(map[string][]TagMeta, 0), make([]spiderhelper.TagURIInfo, 0), make([]*sync.RWMutex, 2)}
	for k := range p.lock {
		p.lock[k] = new(sync.RWMutex)
	}
	return p
}

// OnRequestCallback .
func (p *TagRawData) OnRequestCallback(c *colly.Collector, r *colly.Request) {

}

// OnErrorCallback .
func (p *TagRawData) OnErrorCallback(c *colly.Collector, r *colly.Response, e error) {
	// save error except non-existent pages
	if r.StatusCode != 404 {
		errURI := ccutility.RemoveLastSlash(r.Request.URL.Scheme + "://" + r.Request.URL.Host + r.Request.URL.Path)

		var idx float64
		IdxStr := r.Ctx.GetAny("Idx")
		if IdxStr != nil {
			idx = IdxStr.(float64)
		}

		p.lock[0].Lock()
		p.CountFail++
		p.PageError = append(p.PageError, spiderhelper.TagURIInfo{Idx: uint32(idx), URI: errURI})
		p.lock[0].Unlock()

		config.RedisPool.Exec("HMSET", config.GlobalErrorRawMeta, errURI, strconv.Itoa(r.StatusCode)+"|"+e.Error())
	}
	//r.Request.Retry()
}

// OnResponseCallback .
func (p *TagRawData) OnResponseCallback(c *colly.Collector, r *colly.Response) {
	pageURI := ccutility.RemoveLastSlash(r.Request.URL.Scheme + "://" + r.Request.URL.Host + r.Request.URL.Path)

	p.lock[1].Lock()
	p.CountSucc++
	meta := &TagMeta{len(r.Body), r.Body}
	p.RawMeta[pageURI] = append(p.RawMeta[pageURI], *meta)
	p.lock[1].Unlock()
}

// OnHTMLCallback .
func (p *TagRawData) OnHTMLCallback(c *colly.Collector, r *colly.HTMLElement) {

}

// OnXMLCallback .
func (p *TagRawData) OnXMLCallback(c *colly.Collector, r *colly.XMLElement) {

}

// OnScrapedCallback .
func (p *TagRawData) OnScrapedCallback(c *colly.Collector, r *colly.Response) {
	pageURI := ccutility.RemoveLastSlash(r.Request.URL.Scheme + "://" + r.Request.URL.Host + r.Request.URL.Path)

	config.RedisPool.Exec("HDEL", config.GlobalErrorRawMeta, pageURI)

	//p.lock[1].RLock()
	//if p.Meta[p.pageURI] != nil {
	//	Config.RedisPool.Exec("SADD", pageURI, p.Meta[pageURI])
	//}
	//p.lock[1].RUnlock()
}
