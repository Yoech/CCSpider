package logic

import (
	"bytes"
	"fmt"
	"github.com/Yoech/CCSpider/cccompress"
	"github.com/Yoech/CCSpider/ccutility"
	"github.com/Yoech/CCSpider/ccyaml"
	"github.com/Yoech/CCSpider/config"
	"github.com/Yoech/CCSpider/dbhelper"
	"github.com/Yoech/CCSpider/spiderhelper"
	"github.com/gomodule/redigo/redis"
	"io"
	"log"
	"math"
	"os"
	"sort"
	"strings"
	"sync"
	"time"
)

// Run crawl in the loop or in test mode.
func Run(bTestMode bool) {
	var wg = &sync.WaitGroup{}
	wg.Add(1)
	if bTestMode {
		go func(WG *sync.WaitGroup) {
			defer wg.Done()
			SinglePageCrawler("https://www.mzitu.com/97908")
		}(wg)
	} else {
		Worker()
	}
	wg.Wait()
}

// Worker .
func Worker() {
	ccutility.WorkerTimer(time.Minute*10, func() {
		for _, r := range ccyaml.YamRules {
			Search(r.Site.URI, r, true, false)
		}
	})
}

// Search .
func Search(uri string, r *ccyaml.TagYamlRulesInfo, comparePageTotal bool, ignorePageTotalZero bool) (int, int) {
	config.GLock.Lock()
	defer config.GLock.Unlock()

	var updateCatesList = make([]int64, 0)
	var updateTagsList = make([]int64, 0)
	var updateIDList = make([]int64, 0)
	var stats = config.TagStatsInfo{}

	log.Printf("*****************************************")
	log.Printf("Search.Begin...")
	log.Printf("*****************************************")

	var err error
	var val []interface{}
	var pageTotal, pageTotalCache = 0, 0

	// query page total from cache first
	if comparePageTotal {
		val, err = redis.Values(config.RedisPool.Exec("HMGET", r.Site.URI, "pageTotal"))
		if err == nil && (val != nil && len(val) > 0) {
			pageTotalCache, err = redis.Int(val[0], err)
			if err != nil {
				log.Printf("redis.Int[%v].err[%v]", val[0], err)
			}
		}
	}

	// query page total
	for {
		realURI := r.Site.URI
		if uri != "" {
			realURI = uri
		}
		pageTotal, err = QueryPageTotal(r.Site.Host, realURI, r.Page.NavLink, r.Page.PageNum, true)
		if err != nil {
			log.Printf("QueryPageTotal.err[%v]", err.Error())
			continue
		}

		if ignorePageTotalZero {
			break
		}

		// query page list from range for pages' total
		if pageTotal == 0 {
			log.Printf("QueryPageTotal.pageTotal.nil")
			continue
		}
		break
	}

	// calculate their difference
	pageTotalCache = pageTotal - pageTotalCache + 1

	//// for debugging
	//// Cool.Cat
	//pageTotalCache = 1

	realPageURI := r.Page.URI
	//if uri != "" {
	//	realPageURI = uri + "/page"
	//}

	// query total page detail list
	pageDetails, indexDetails := QueryPageList(r.Site.Host, realPageURI, pageTotalCache, r.Page.PageList, r.Page.PageListAttr, r.Page.IndexImage, r.Page.IndexImageAttr, r.Page.IndexImageRegexp, true)

	// loop page hash(Title,Link) and save images raw data to physical disk and cache
	totalURICount, totalRawCount := QueryRawURI(pageDetails, indexDetails, r.Site.Host, r.Page.Category, r.Page.CategoryURI, r.Page.Tags, r.Page.TagsURI, r.Content.NavLink, r.Content.PageNum, r.Content.RawData, &updateIDList, &updateCatesList, &updateTagsList)

	stats.TotalURISucc += totalURICount
	stats.TotalRawSucc += totalRawCount

	// update stats
	if stats.TotalURISucc > 0 && stats.TotalRawSucc > 0 {
		config.RedisPool.Exec("HINCRBY", r.Site.URI, "updateTotalUri", totalURICount)
		config.RedisPool.Exec("HINCRBY", r.Site.URI, "updateTotalRaw", totalRawCount)

		stats.LastUpdateTime = time.Now().In(time.FixedZone("CST", 8*3600)).Format(config.GlobalBaseTime)
		config.GStats = append(config.GStats, stats)
	}

	// post notifity to web server
	bNeedUpdateIndex := false
	if len(updateIDList) > 0 {
		for _, v := range updateIDList {
			config.RedisPool.Exec("SADD", config.GlobalGenerateDetails, v)
		}
		bNeedUpdateIndex = true
	}
	if len(updateCatesList) > 0 {
		for _, v := range updateCatesList {
			config.RedisPool.Exec("SADD", config.GlobalGenerateCate, v)
		}
	}
	if len(updateTagsList) > 0 {
		for _, v := range updateTagsList {
			config.RedisPool.Exec("SADD", config.GlobalGenerateTag, v)
		}
	}
	if bNeedUpdateIndex {
		config.RedisPool.Exec("HMSET", config.GlobalGeneratePage, r.Site.URI, time.Now().Format("2006-01-02 15:04:05"))
	}

	log.Printf("*****************************************")
	log.Printf("Search...Finished.[UPDATED][%v] => %v/%v", stats.LastUpdateTime, stats.TotalURISucc, stats.TotalRawSucc)
	log.Printf("*****************************************")

	return stats.TotalURISucc, stats.TotalRawSucc
}

// SinglePageCrawler .
func SinglePageCrawler(uri string) {
	var testMap = make(map[string][]string)
	testMap[uri] = append(testMap[uri], "美女模特雅雯身材惹火长相甜美 超长美腿堪称极品1", uri)

	if len(ccyaml.YamRules) == 0 {
		return
	}
	v := ccyaml.YamRules[0]
	var updateCatesList []int64
	var updateTagsList []int64
	var updateIDList []int64
	QueryRawURI(testMap, nil, v.Site.Host, v.Page.Category, v.Page.CategoryURI, v.Page.Tags, v.Page.TagsURI, v.Content.NavLink, v.Content.PageNum, v.Content.RawData, &updateIDList, &updateCatesList, &updateTagsList)
}

// QueryPageTotal .
func QueryPageTotal(indexURI string, hostURI string, navLink string, pageNumber string, bSave2Redis bool) (int, error) {
	s := time.Now()

	p := AllocPageTotal(indexURI, hostURI, navLink, pageNumber, bSave2Redis)

	var uriInfo = &spiderhelper.TagURIInfo{Idx: 0, URI: p.indexURI}
	err := spiderhelper.RunColly(p.hostURI, []spiderhelper.TagURIInfo{*uriInfo},
		nil,
		nil,
		p.OnResponseCallback,
		nil,
		nil,
		nil,
	)
	cost := time.Now().Unix() - s.Unix()
	log.Printf("QueryPageTotal.hostURI[%v].cost[%v s]", hostURI, cost)
	return p.PageTotal, err
}

// RemoveRepeatedSlice .
func RemoveRepeatedSlice(slice []spiderhelper.TagURIInfo) (newSlice []spiderhelper.TagURIInfo) {
	newSlice = make([]spiderhelper.TagURIInfo, 0)
	for i := 0; i < len(slice); i++ {
		repeat := false
		for j := i + 1; j < len(slice); j++ {
			if slice[i].Idx == slice[j].Idx && slice[i].URI == slice[j].URI {
				repeat = true
				break
			}
		}
		if !repeat {
			newSlice = append(newSlice, slice[i])
		}
	}
	return
}

// QueryPageList .
func QueryPageList(hostURI string, pageURI string, pageTotal int, pageList string, pageListAttr string, indexImage string, indexImageAttr string, indexImageRegexp []string, bConcurrent bool) (map[string][]string, map[string][]string) {
	if pageTotal < 1 {
		return nil, nil
	}

	s := time.Now()

	p := AllocPageList(hostURI, pageURI, pageTotal, pageList, pageListAttr, indexImage, indexImageAttr, indexImageRegexp, bConcurrent)

	var uriRange = make([]string, 0)
	for i := 1; i < p.pageTotal+1; i++ {
		pageURI := fmt.Sprintf("%v/%v", p.pageURI, i)
		uriRange = append(uriRange, pageURI)
	}

	if p.bConcurrent {
		RunPageWorkerMultiCoroutines(p.hostURI, uriRange, p.pageTotal,
			p.OnRequestCallback,
			p.OnErrorCallback,
			nil,
			[]spiderhelper.TagOnHTMLCallback{
				{
					p.PageList,
					p.OnHTMLCallbackForPageList,
				},
				{
					p.IndexImage,
					p.OnHTMLCallbackForIndexImage,
				},
			},
			nil,
			p.OnScrapedCallback,
			&p.PageError,
		)
	} else {
		RunPageWorkerSingleCoroutines(p.hostURI, uriRange, p.pageTotal,
			p.OnRequestCallback,
			p.OnErrorCallback,
			nil,
			[]spiderhelper.TagOnHTMLCallback{
				{
					p.PageList,
					p.OnHTMLCallbackForPageList,
				},
				{
					p.IndexImage,
					p.OnHTMLCallbackForIndexImage,
				},
			},
			nil,
			p.OnScrapedCallback,
		)
	}

	cost := time.Now().Unix() - s.Unix()
	log.Printf("QueryPageList.cost[%v s]", cost)
	return p.PageDetails, p.IndexDetails
}

// QueryRawURI .
func QueryRawURI(pageDetails map[string][]string, indexDetails map[string][]string, hostURI string, cateFilter string, cateURIFilter string, tagsFilter string, tagsURIFilter string, navLinkFilter string, pageNumFilter string, rawDataFilter string, updateIDList *[]int64, updateCatesList *[]int64, updateTagsList *[]int64) (totalURICount int, totalRawCount int) {
	totalPageList := len(pageDetails)
	if totalPageList < 1 {
		return
	}
	totalIndexImage := len(indexDetails)
	if totalIndexImage != totalPageList {
		log.Printf("QueryRawURI.totalPageList[%v].totalIndexImage[%v].no match", totalPageList, totalIndexImage)
		return
	}
	log.Printf("QueryRawURI.begin...totalPageList=%v", totalPageList)
	s := time.Now()

	//var lockRaw = new(sync.RWMutex)

	// 224
	for k, v := range pageDetails {
		// 24
		m := len(v) / 2

		indexDetailsSlice := indexDetails[k]

		//ch := make(chan int, m)
		//var wg = &sync.WaitGroup{}

		for j := 0; j < m; j++ {
			title := v[j*2]
			uri := v[j*2+1]
			var indexImage string
			for k, y := range indexDetailsSlice {
				if y == uri {
					indexImage = indexDetailsSlice[k+1]
					break
				}
			}

			if len(indexImage) == 0 {
				log.Printf("QueryRawURI.m[%v/%v].title[%v].uri[%v].indexImage.nil", j, m, title, uri)
				break
			}
			log.Printf("QueryRawURI.m[%v/%v].title[%v].uri[%v].indexImage[%v]", j, m, title, uri, indexImage)

			lastID, catesID, tagsID, rawCount := QuerySingleRawURI(hostURI, title, uri, indexImage, cateFilter, cateURIFilter, tagsFilter, tagsURIFilter, navLinkFilter, pageNumFilter, rawDataFilter)

			totalURICount++
			totalRawCount += rawCount
			if lastID > 0 {
				*updateIDList = append(*updateIDList, lastID)
			}
			if catesID > 0 {
				*updateCatesList = append(*updateCatesList, catesID)
			}
			for _, v := range tagsID {
				if v > 0 {
					*updateTagsList = append(*updateTagsList, v)
				}
			}

			//wg.Add(1)
			//go func(ch <-chan int, wg *sync.WaitGroup, host string, title string, uri string, index string, cateFilter string, cateURIFilter string, tagsFilter string, tagsURIFilter string, navLinkFilter string, pageNumFilter string, rawDataFilter string, updateIDList *[]int64, updateCatesList *[]int64, updateTagsList *[]int64) {
			//	defer wg.Done()
			//	lastID, catesID, tagsID, rawCount := QuerySingleRawURI(host, title, uri, index, cateFilter, cateURIFilter, tagsFilter, tagsURIFilter, navLinkFilter, pageNumFilter, rawDataFilter)
			//
			//	lockRaw.Lock()
			//	totalURICount++
			//	totalRawCount += rawCount
			//	if lastID > 0 {
			//		*updateIDList = append(*updateIDList, lastID)
			//	}
			//	if catesID > 0 {
			//		*updateCatesList = append(*updateCatesList, catesID)
			//	}
			//	for _, v := range tagsID {
			//		if v > 0 {
			//			*updateTagsList = append(*updateTagsList, v)
			//		}
			//	}
			//	lockRaw.Unlock()
			//}(ch, wg, hostURI, title, uri, indexImage, cateFilter, cateURIFilter, tagsFilter, tagsURIFilter, navLinkFilter, pageNumFilter, rawDataFilter, updateIDList, updateCatesList, updateTagsList)
			//ch <- 1
			//
			//// 5s per element
			//time.Sleep(time.Duration(1) * time.Second)
		}
		//wg.Wait()
	}

	e := time.Now()
	diff := e.Unix() - s.Unix()
	log.Printf("QueryRawURI.end.cost[%vs].totalURICount[%v].totalRawCount[%v]", diff, totalURICount, totalRawCount)
	return totalURICount, totalRawCount
}

// QuerySingleRawURI .
func QuerySingleRawURI(hostURI string, title string, uri string, indexImage string, cateFilter string, cateURIFilter string, tagsFilter string, tagsURIFilter string, navLinkFilter string, pageNumFilter string, rawDataFilter string) (lastID int64, updateCates int64, updateTags []int64, rawCount int) {
	var err error
	var pageTotal = 0
	var reply interface{}
	for {
		// query total page for current uri
		pageTotal, err = QueryPageTotal(hostURI, uri, navLinkFilter, pageNumFilter, false)
		if err != nil {
			log.Printf("QuerySingleRawURI.uri[%v].err[%v]", uri, err.Error())
			time.Sleep(time.Duration(1) * time.Second)
			continue
		}
		if pageTotal == 0 {
			log.Printf("QuerySingleRawURI.uri[%v].pageCount=0", uri)
			time.Sleep(time.Duration(1) * time.Second)
			continue
		}
		break
	}

	// if uri exist in cache and total is equal.then ignore them
	reply, err = config.RedisPool.Exec("SCARD", uri)
	if err == nil && reply.(int64) == int64(pageTotal) {
		log.Printf("QuerySingleRawURI.uri[%v].exists...ignore!", uri)
		return 0, 0, nil, 0
	}

	//time.Sleep(time.Duration(1) * time.Second)

	// query content uri
	var rawCate, rawTags, rawURI map[string][]string
	for {
		rawCate, rawTags, rawURI = OnQueryRawURI(hostURI, uri, pageTotal, cateFilter, cateURIFilter, tagsFilter, tagsURIFilter, rawDataFilter)
		if rawCate == nil || rawTags == nil || rawURI == nil {
			log.Printf("QuerySingleRawURI.uri[%v].nil.try again!", uri)
			time.Sleep(time.Duration(1) * time.Second)
			continue
		}
		break
	}

	var rangeURI = make([]string, 0)
	for _, y := range rawURI {
		rangeURI = append(rangeURI, y...)
	}

	// category/tags duplication,only get one
	var category string
	var categoryURI string
	for k := range rawCate {
		category = rawCate[k][0]
		categoryURI = rawCate[k][1]
		break
	}
	var tags = make([]string, 0)
	for k := range rawTags {
		tags = rawTags[k]
		break
	}

	// query database/cache and update them
	cateID, tagsID, tagsIDStr := dbhelper.DBHelper.QueryCategoryAndTags(category, categoryURI, tags)

	// query every raw data and make data landing
	if len(rangeURI) > 0 {
		lastID, rawCount, _ = QueryRawData(hostURI, title, cateID, tagsIDStr, uri, indexImage, rangeURI)
	}
	log.Printf("QuerySingleRawURI.uri[%v].lastID[%v].rawCount[%v]...Done!", uri, lastID, rawCount)
	return lastID, cateID, tagsID, rawCount
}

// SaveIndexImage .
func SaveIndexImage(hostURI string, pageURI string, indexImage string) string {
	path := strings.Split(indexImage, "/")
	if len(path) < 3 {
		return indexImage
	}

	dir := path[3 : len(path)-1]
	dirs := ccyaml.YamlConf.Info.Storage + "/" + strings.Replace(strings.Trim(fmt.Sprint(dir), "[]"), " ", "/", -1) + "/"
	name := path[len(path)-1]
	realPath := "/" + strings.Replace(strings.Trim(fmt.Sprint(path[3:]), "[]"), " ", "/", -1)

	// check files exists
	bExist, _ := ccutility.PathExists(dirs + name)
	if bExist == true {
		log.Printf("SaveIndexImage.indexImage[%v][%v]...exists.return.realPath[%v]!", indexImage, dirs+name, realPath)
		return realPath
	}

	// query raw data
	indexSlice := make([]string, 0)
	indexSlice = append(indexSlice, indexImage)
	length, data := OnQueryRawData(hostURI, pageURI, indexSlice)
	if length < 1 {
		log.Printf("SaveIndexImage.QueryRawData[%v].data.nil", pageURI)
		return indexImage
	}

	var err error
	var bSaved = false

	for k, v := range data {
		path := strings.Split(k, "/")
		if len(path) < 3 {
			continue
		}

		//key := "/" + strings.Replace(strings.Trim(fmt.Sprint(path[3:len(path)]), "[]"), " ", "/", -1)
		dirs := ccyaml.YamlConf.Info.Storage + "/" + strings.Replace(strings.Trim(fmt.Sprint(path[3:len(path)-1]), "[]"), " ", "/", -1) + "/"
		name := path[len(path)-1]

		dst := v[0].Data
		if ccyaml.YamlConf.Info.ConfusionEnabled == true {
			dst, err = cccompress.Compress(ccyaml.YamlConf.Info.ConfusionKey, v[0].Data, cccompress.Zlib)
			if err != nil {
				continue
			}
		}

		err = os.MkdirAll(dirs, os.ModePerm)
		if err != nil {
			log.Printf("os.MkdirAll[%v].err[%v]", dirs, err)
			continue
		}

		fs, err := os.Create(dirs + name)
		if err != nil {
			log.Printf("os.Create[%v].err[%v]", dirs+name, err)
			continue
		}

		_, err = io.Copy(fs, bytes.NewReader(dst))
		if err != nil {
			log.Printf("io.Copy[%v].err[%v]", dirs+name, err)

			err = fs.Close()
			if err != nil {
				log.Printf("Copy.Close[%v].err[%v]", dirs+name, err)
			}
			continue
		}

		err = fs.Close()
		if err != nil {
			log.Printf("Create.Close[%v].err[%v]", dirs+name, err)
			continue
		}

		bSaved = true
	}

	if bSaved {
		return realPath
	}
	return indexImage
}

// QueryRawData .
func QueryRawData(hostURI string, pageTitle string, cateID int64, tagsID string, pageURI string, indexImage string, rangeURI []string) (lastID int64, rawCount int, err error) {
	//log.Printf("QueryRawData.begin...")
	s := time.Now()

	indexImage = SaveIndexImage(hostURI, pageURI, indexImage)

	// if a separate element already exists on the local disk, then ignored it
	var realRangeURI = make([]string, 0)
	for _, v := range rangeURI {
		path := strings.Split(v, "/")
		if len(path) < 3 {
			continue
		}

		dir := path[3 : len(path)-1]
		dirs := ccyaml.YamlConf.Info.Storage + "/" + strings.Replace(strings.Trim(fmt.Sprint(dir), "[]"), " ", "/", -1) + "/"
		name := path[len(path)-1]

		// check files exists
		bExist, _ := ccutility.PathExists(dirs + name)
		if bExist == true {
			//log.Printf("PathExists[%v]....Exist", dirs+name)

			// update cache
			config.RedisPool.Exec("SADD", pageURI, v)

			rawCount++
			continue
		}

		realRangeURI = append(realRangeURI, v)
	}

	if len(realRangeURI) == 0 && len(rangeURI) == rawCount {
		log.Printf("QueryRawData[%v].indexImage[%v].realRangeURI.zero", pageURI, indexImage)

		lastID = dbhelper.DBHelper.SaveThumbs2DB(pageTitle, pageURI, indexImage, rangeURI)

		// save raw data to database and cache
		sort.Strings(rangeURI)
		lastID = dbhelper.DBHelper.SaveRawInfo2DB(pageTitle, cateID, tagsID, pageURI, indexImage, rangeURI)

		return lastID, 0, fmt.Errorf("QueryRawData[%v].realRangeURI.zero", pageURI)
	}

	// query raw data
	length, data := OnQueryRawData(hostURI, pageURI, realRangeURI)
	if length < 1 {
		log.Printf("QueryRawData[%v].data.nil", pageURI)
		return 0, 0, fmt.Errorf("QueryRawData[%v].data.nil", pageURI)
	}

	// save raw data to database and cache
	sort.Strings(rangeURI)
	lastID = dbhelper.DBHelper.SaveRawInfo2DB(pageTitle, cateID, tagsID, pageURI, indexImage, rangeURI)

	for k, v := range data {
		path := strings.Split(k, "/")
		if len(path) < 3 {
			continue
		}

		//key := "/" + strings.Replace(strings.Trim(fmt.Sprint(path[3:len(path)]), "[]"), " ", "/", -1)
		dirs := ccyaml.YamlConf.Info.Storage + "/" + strings.Replace(strings.Trim(fmt.Sprint(path[3:len(path)-1]), "[]"), " ", "/", -1) + "/"
		name := path[len(path)-1]

		//// check files exists
		//bExist, _ := ccutility.PathExists(dirs + name)
		//if bExist == true {
		//	log.Printf("PathExists[%v]....Exist", dirs+name)
		//
		//	// update cache
		//	config.RedisPool.Exec("SADD", pageURI, k)
		//
		//	rawCount++
		//	continue
		//}

		dst := v[0].Data
		if ccyaml.YamlConf.Info.ConfusionEnabled == true {
			dst, err = cccompress.Compress(ccyaml.YamlConf.Info.ConfusionKey, v[0].Data, cccompress.Zlib)
			if err != nil {
				continue
			}
		}

		err = os.MkdirAll(dirs, os.ModePerm)
		if err != nil {
			log.Printf("os.MkdirAll[%v].err[%v]", dirs, err)
			continue
		}

		fs, err := os.Create(dirs + name)
		if err != nil {
			log.Printf("os.Create[%v].err[%v]", dirs+name, err)
			continue
		}

		_, err = io.Copy(fs, bytes.NewReader(dst))
		if err != nil {
			log.Printf("io.Copy[%v].err[%v]", dirs+name, err)

			err = fs.Close()
			if err != nil {
				log.Printf("Copy.Close[%v].err[%v]", dirs+name, err)
			}
			continue
		}

		err = fs.Close()
		if err != nil {
			log.Printf("Create.Close[%v].err[%v]", dirs+name, err)
			continue
		}

		// update cache
		config.RedisPool.Exec("SADD", pageURI, k)

		rawCount++
	}

	e := time.Now()
	diff := e.Unix() - s.Unix()
	log.Printf("QueryRawData.end.pageURI[%v].cost[%vs]", pageURI, diff)
	return lastID, rawCount, err
}

// OnQueryRawURI .
func OnQueryRawURI(hostURI string, pageURI string, pageTotal int, cateFilter string, cateURIFilter string, tagsFilter string, tagsURIFilter string, rawDataFilter string) (map[string][]string, map[string][]string, map[string][]string) {
	if pageTotal < 1 {
		return nil, nil, nil
	}

	var uriRange = make([]string, 0)
	for i := 1; i < pageTotal+1; i++ {
		uriRange = append(uriRange, fmt.Sprintf("%v/%v", pageURI, i))
	}

	p := AllocRawURI(pageURI, cateFilter, cateURIFilter, tagsFilter, tagsURIFilter, rawDataFilter)
	RunPageWorkerMultiCoroutines(hostURI, uriRange, pageTotal,
		p.OnRequestCallback,
		p.OnErrorCallback,
		nil,
		[]spiderhelper.TagOnHTMLCallback{
			{
				p.RawURIFilter,
				p.OnHTMLCallbackForRawURI,
			},
			{
				p.cateFilter,
				p.OnHTMLCallbackForRawCate,
			},
			{
				p.tagsFilter,
				p.OnHTMLCallbackForRawTags,
			},
		},
		nil,
		p.OnScrapedCallback,
		&p.PageError,
	)
	return p.RawCate, p.RawTags, p.RawURI
}

// OnQueryRawData .
func OnQueryRawData(hostURI string, pageURI string, imgRange []string) (int, map[string][]TagMeta) {
	//log.Printf("OnQueryRawData.begin...")
	//s := time.Now()

	var uriRange = make([]spiderhelper.TagURIInfo, 0)
	for k, v := range imgRange {
		uriRange = append(uriRange, spiderhelper.TagURIInfo{Idx: uint32(k), URI: v})
	}

	p := AllocRawData(pageURI)
	total := len(uriRange)
	RunPageWorkerMultiCoroutines(hostURI, imgRange, total,
		nil,
		p.OnErrorCallback,
		p.OnResponseCallback,
		nil,
		nil,
		p.OnScrapedCallback,
		&p.PageError,
	)
	//e := time.Now()
	//diff := e.Unix() - s.Unix()
	//log.Printf("OnQueryRawData.end.cost[%vs]", diff)
	return len(p.RawMeta), p.RawMeta
}

// RunPageWorkerSingleCoroutines .
func RunPageWorkerSingleCoroutines(hostURI string, uriRange []string, pageTotal int, cbRequest spiderhelper.OnRequestCallback, cbError spiderhelper.OnErrorCallback, cbResponse spiderhelper.OnResponseCallback, cbHTMLs []spiderhelper.TagOnHTMLCallback, cbXMLs []spiderhelper.TagOnXMLCallback, cbScraped spiderhelper.OnScrapedCallback) {
	//log.Printf("RunPageWorkerSingle.begin...")
	s := time.Now()
	var pageRange []spiderhelper.TagURIInfo
	for idx := 1; idx < pageTotal+1; idx++ {
		pageItem := &spiderhelper.TagURIInfo{Idx: uint32(idx), URI: uriRange[idx]}
		pageRange = append(pageRange, *pageItem)
	}
	RunPageWorker(hostURI, pageRange, cbRequest, cbError, cbResponse, cbHTMLs, cbXMLs, cbScraped)
	e := time.Now()
	diff := e.Unix() - s.Unix()
	log.Printf("RunPageWorkerSingle.end.cost[%vs]", diff)
	return
}

// RunPageWorkerMultiCoroutines .
func RunPageWorkerMultiCoroutines(hostURI string, uriRange []string, pageTotal int, cbRequest spiderhelper.OnRequestCallback, cbError spiderhelper.OnErrorCallback, cbResponse spiderhelper.OnResponseCallback, cbHTMLs []spiderhelper.TagOnHTMLCallback, cbXMLs []spiderhelper.TagOnXMLCallback, cbScraped spiderhelper.OnScrapedCallback, pageError *[]spiderhelper.TagURIInfo) {
	//log.Printf("RunPageWorkerMultiCoroutines.begin...")
	//s := time.Now()

	// core worker
	workerTotal := config.GlobalWorkerNum

	// number of pages per worker
	pagePerCPU := 1
	if pageTotal > workerTotal {
		f := math.Ceil(float64(pageTotal) / float64(workerTotal))
		pagePerCPU = ccutility.Round(f)
	} else {
		workerTotal = pageTotal
		pagePerCPU = 1
	}

	ch := make(chan int, workerTotal)
	var wg = &sync.WaitGroup{}

	for i := 0; i < workerTotal; i++ {
		wg.Add(1)
		go func(ch <-chan int, wg *sync.WaitGroup, i int, p int, host string, cbRequest spiderhelper.OnRequestCallback, cbError spiderhelper.OnErrorCallback, cbResponse spiderhelper.OnResponseCallback, cbHTMLs []spiderhelper.TagOnHTMLCallback, cbXMLs []spiderhelper.TagOnXMLCallback, cbScraped spiderhelper.OnScrapedCallback) {
			defer wg.Done()
			var pageRange = make([]spiderhelper.TagURIInfo, 0)
			for idx := i * pagePerCPU; idx < (i+1)*pagePerCPU; idx++ {
				if idx >= pageTotal {
					break
				}
				pageItem := &spiderhelper.TagURIInfo{Idx: uint32(idx), URI: uriRange[idx]}
				pageRange = append(pageRange, *pageItem)
			}
			RunPageWorker(host, pageRange, cbRequest, cbError, cbResponse, cbHTMLs, cbXMLs, cbScraped)
		}(ch, wg, i, pagePerCPU, hostURI, cbRequest, cbError, cbResponse, cbHTMLs, cbXMLs, cbScraped)
		ch <- 1
		//time.Sleep(time.Duration(1) * time.Second)
	}
	wg.Wait()

	//e := time.Now()
	//diff1 := e.Unix() - s.Unix()

	// 再处理未成功的
	loopCount := 0
	for {
		pageErrorCount := len(*pageError)
		if pageErrorCount == 0 {
			break
		}

		// 去重
		var tmp []spiderhelper.TagURIInfo
		tmp = RemoveRepeatedSlice(*pageError)
		*pageError = tmp
		pageErrorCount = len(*pageError)

		loopCount++
		pageTotal = pageErrorCount
		pageLeft := 0

		if pageTotal > workerTotal {
			f := math.Floor(float64(pageTotal) / float64(workerTotal))
			pagePerCPU = ccutility.Round(f)
			pageLeft = pageTotal % workerTotal
		} else {
			// left pages not enough full workers.so we let worker's num equial left page's num.
			// Cool.Cat
			workerTotal = pageTotal
			pagePerCPU = 1
		}

		//log.Printf("workerTotal[%v].pageErrorCount[%v].pagePerCPU[%v].pageLeft[%v].loopCount[%v]", workerTotal, pageErrorCount, pagePerCPU, pageLeft, loopCount)

		// padding
		leftSlice := make([][]spiderhelper.TagURIInfo, workerTotal)
		//log.Printf("+---------------------------------+")
		for i := 1; i < workerTotal+1; i++ {
			leftSlice[i-1] = make([]spiderhelper.TagURIInfo, pagePerCPU)
			copy(leftSlice[i-1], (*pageError)[(i-1)*pagePerCPU:i*pagePerCPU])
			//log.Printf("leftSlice[%v]=%v", i-1, leftSlice[i-1])
		}
		//log.Printf("+---------------------------------+")
		for i := 0; i < pageLeft; i++ {
			leftSlice[i] = append(leftSlice[i], (*pageError)[workerTotal*pagePerCPU+i])
			//log.Printf("leftSlice[%v]=%v", i, leftSlice[i])
		}
		//log.Printf("+---------------------------------+")

		// clean
		*pageError = (*pageError)[0:0]

		ch2 := make(chan int, workerTotal)
		var wg2 = &sync.WaitGroup{}

		for i := 0; i < workerTotal; i++ {
			wg2.Add(1)
			go func(ch2 <-chan int, wg2 *sync.WaitGroup, i int, host string, s []spiderhelper.TagURIInfo, cbRequest spiderhelper.OnRequestCallback, cbError spiderhelper.OnErrorCallback, cbResponse spiderhelper.OnResponseCallback, cbHTMLs []spiderhelper.TagOnHTMLCallback, cbXMLs []spiderhelper.TagOnXMLCallback, cbScraped spiderhelper.OnScrapedCallback) {
				defer func() {
					wg2.Done()
				}()
				RunPageWorker(host, s, cbRequest, cbError, cbResponse, cbHTMLs, cbXMLs, cbScraped)
			}(ch2, wg2, i, hostURI, leftSlice[i], cbRequest, cbError, cbResponse, cbHTMLs, cbXMLs, cbScraped)
			ch2 <- 1
			//time.Sleep(time.Duration(1) * time.Second)
		}
		wg2.Wait()
	}

	//s = time.Now()
	//diff2 := s.Unix() - e.Unix()
	//log.Printf("RunPageWorkerMultiCoroutines.end.cost[%vs/%vs].loopCount[%v]", diff1, diff2, loopCount)
}

// RunPageWorker .
func RunPageWorker(hostURI string, uriRange []spiderhelper.TagURIInfo, cbRequest spiderhelper.OnRequestCallback, cbError spiderhelper.OnErrorCallback, cbResponse spiderhelper.OnResponseCallback, cbHTMLs []spiderhelper.TagOnHTMLCallback, cbXMLs []spiderhelper.TagOnXMLCallback, cbScraped spiderhelper.OnScrapedCallback) {

	if uriRange == nil || len(uriRange) == 0 {
		return
	}

	_ = spiderhelper.RunColly(hostURI, uriRange,
		cbRequest,
		cbError,
		cbResponse,
		cbHTMLs,
		cbXMLs,
		cbScraped,
	)
}
