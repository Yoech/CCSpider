package dbhelper

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/Yoech/CCSpider/ccutility"
	"github.com/Yoech/CCSpider/config"
	"github.com/gomodule/redigo/redis"
	"io/ioutil"
	"log"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

// TagDBHelperInfo .
type TagDBHelperInfo struct {
	mapCate  map[string]int64
	mapTags  map[string]int64
	lockCate *sync.RWMutex
	lockTags *sync.RWMutex
}

// DBHelper .
var DBHelper *TagDBHelperInfo

func init() {
	if DBHelper != nil {
		return
	}

	DBHelper = &TagDBHelperInfo{
		make(map[string]int64, 0),
		make(map[string]int64, 0),
		new(sync.RWMutex),
		new(sync.RWMutex),
	}
}

// Rebuild .
func (p *TagDBHelperInfo) Rebuild(sqlpath string) bool {
	if config.MySQL == nil {
		log.Printf("Rebuild.MySQL.nil")
		return false
	}

	var err error

	if sqlpath == "" {
		return false
	}

	sqlFile, err := ioutil.ReadFile(sqlpath)
	if err != nil {
		log.Printf("Rebuild.ReadFile[%v].err[%v]", sqlpath, err)
		return false
	}

	sqlData := string(sqlFile)

	// 'DELIMITER' is a client-side feature.The server-side doesn't recognize this character.
	// so we replace 'DELIMITER' to empty string.and replace '$$' to ';'
	// Cool.Cat@2019-08-05 20:12:47
	sqlData = strings.Replace(strings.Replace(strings.Replace(sqlData, "DELIMITER $$", "", -1), "DELIMITER ;", "", -1), "$$", ";", -1)

	tx, _ := config.MySQL.Begin()
	defer tx.Rollback()

	_, err = tx.Exec(sqlData)
	if err != nil {
		log.Printf("Rebuild.Exec.err[%v]", err)
		return false
	}

	if err = tx.Commit(); err != nil {
		log.Printf("Rebuild.Commit.err[%v]", err)
		return false
	}

	return true
}

// ReloadCategoryAndTags .
func (p *TagDBHelperInfo) ReloadCategoryAndTags() {
	p.lockCate.Lock()
	p.mapCate = make(map[string]int64, 0)
	p.lockCate.Unlock()

	p.lockTags.Lock()
	p.mapTags = make(map[string]int64, 0)
	p.lockTags.Unlock()

	resultCate, err := config.MySQL.Query("select Rid,CateName from ccspider_category")
	if err == nil {
		p.lockCate.Lock()
		for _, v := range resultCate {
			key := v["Rid"].(int64)
			val := v["CateName"].(string)
			p.mapCate[val] = key
		}
		p.lockCate.Unlock()

		p.lockCate.RLock()
		if len(p.mapCate) > 0 {
			config.RedisPool.Exec("HMSET", redis.Args{}.Add(config.GlobalURICate).AddFlat(p.mapCate)...)
		}
		p.lockCate.RUnlock()
	}

	resultTags, err := config.MySQL.Query("select Rid,TagName from ccspider_tags")
	if err == nil {
		p.lockTags.Lock()
		for _, v := range resultTags {
			key := v["Rid"].(int64)
			val := v["TagName"].(string)
			p.mapTags[val] = key
		}
		p.lockTags.Unlock()

		p.lockTags.RLock()
		if len(p.mapTags) > 0 {
			config.RedisPool.Exec("HMSET", redis.Args{}.Add(config.GlobalURITags).AddFlat(p.mapTags)...)
		}
		p.lockTags.RUnlock()
	}
}

// QueryCategoryAndTags .
func (p *TagDBHelperInfo) QueryCategoryAndTags(category string, categoryURI string, tags []string) (int64, []int64, string) {
	p.lockCate.RLock()
	cateID, err := p.mapCate[category]
	p.lockCate.RUnlock()
	if err == false {
		cateID, _ = p.SaveCategory2DB(category, categoryURI)
		if cateID > 0 {
			// update cache
			p.lockCate.Lock()
			p.mapCate[category] = cateID
			p.lockCate.Unlock()
		}
	}

	var b strings.Builder
	var c []string
	var uris [] string
	p.lockTags.RLock()
	//for _, v := range tags {
	for i := 0; i < len(tags)/2; i++ {
		v := tags[i*2]
		tagID, err := p.mapTags[v]
		if err == false {
			c = append(c, v)
			uris = append(uris, tags[i*2+1])
		} else {
			b.WriteString(fmt.Sprintf("%v,", tagID))
		}
	}
	p.lockTags.RUnlock()

	// update cache
	if len(c) > 0 {
		d, err := p.SaveTags2DB(c, uris)
		if err == nil {
			p.lockTags.Lock()
			for k, v := range d {
				p.mapTags[k] = v
				b.WriteString(fmt.Sprintf("%v,", v))
			}
			p.lockTags.Unlock()
		}
	}

	var tagsStr string
	if b.Len() > 0 {
		tagsStr = strings.TrimRight(b.String(), ",")
	}

	// finally,we sort tags order by asc
	// Cool.Cat
	tagSlice := strings.Split(tagsStr, ",")
	sort.Strings(tagSlice)
	sort.SliceStable(tagSlice, func(i, j int) bool {
		x, _ := strconv.Atoi(tagSlice[i])
		y, _ := strconv.Atoi(tagSlice[j])
		return x < y
	})
	tagsStr = fmt.Sprint(tagSlice)

	tagsStr = strings.Replace(strings.Trim(tagsStr, "[]"), " ", ",", -1)

	tagsID := make([]int64, 0)
	for _, v := range tagSlice {
		h, e := strconv.ParseInt(v, 10, 64)
		if e == nil {
			tagsID = append(tagsID, h)
		}
	}

	return cateID, tagsID, tagsStr
}

// SaveCategory2DB .
func (p *TagDBHelperInfo) SaveCategory2DB(category string, categoryURI string) (int64, error) {
	if len(category) < 1 {
		return 0, errors.New("category can't empty")
	}

	tx, _ := config.MySQL.Begin()
	defer tx.Rollback()

	var sqlStr = fmt.Sprintf("insert into ccspider_category (CateName,CateUri) values ('%v','%v') ON DUPLICATE KEY UPDATE CateName=VALUES(CateName),CateUri=VALUES(CateUri);", category, categoryURI)

	cateID, err := tx.Insert(sqlStr)
	if err != nil {
		return 0, err
	}

	tx.Commit()

	// return zero because catename exists!
	if cateID == 0 {
		result, err := config.MySQL.Get(fmt.Sprintf("select Rid from ccspider_category where catename='%v'", category))
		if err != nil {
			return 0, err
		}
		cateID = result["Rid"].(int64)
	}

	config.RedisPool.Exec("HMSET", config.GlobalURICate, category, cateID)
	//Config.RedisPool.Exec("HSETNX", Config.GlobalURICate, category, cateId)

	return cateID, nil
}

// SaveTags2DB .
func (p *TagDBHelperInfo) SaveTags2DB(tags []string, uris []string) (map[string]int64, error) {
	if len(tags) < 1 {
		return nil, errors.New("tags can't empty")
	}

	if len(tags) != len(uris) {
		return nil, errors.New("tags/uris no match")
	}

	tx, _ := config.MySQL.Begin()
	defer tx.Rollback()

	var sqlStr strings.Builder
	sqlStr.WriteString("insert into ccspider_tags (TagName,TagUri) values ")

	for k, v := range tags {
		sqlStr.WriteString(fmt.Sprintf("('%v','%v'),", v, uris[k]))
	}

	sqlStr2 := strings.TrimRight(sqlStr.String(), ",")
	sqlStr2 = fmt.Sprintf("%v ON DUPLICATE KEY UPDATE TagName=VALUES(TagName),TagUri=VALUES(TagUri);", sqlStr2)

	_, err := tx.Insert(sqlStr2)
	if err != nil {
		return nil, err
	}

	tx.Commit()

	var b strings.Builder
	b.WriteString("(")
	for _, v := range tags {
		b.WriteString(fmt.Sprintf("'%v',", v))
	}
	str := strings.TrimRight(b.String(), ",")
	str = fmt.Sprintf("%v)", str)

	sqlStr2 = fmt.Sprintf("select Rid,TagName from ccspider_tags where tagName in %v", str)

	result, err := config.MySQL.Query(sqlStr2)
	if err != nil {
		return nil, err
	}

	var ragneTags = make(map[string]int64, 0)
	for _, v := range result {
		key := v["Rid"].(int64)
		val := v["TagName"].(string)
		ragneTags[val] = key
		//Config.RedisPool.Exec("HSETNX", Config.GlobalURITags, val, key)
	}

	if len(ragneTags) > 0 {
		config.RedisPool.Exec("HMSET", redis.Args{}.Add(config.GlobalURITags).AddFlat(ragneTags)...)
	}

	return ragneTags, nil
}

// TagRawInfo use as compact a struct as possible, in order to occupy as little redis memory as possible
// Cool.Cat
type TagRawInfo struct {
	R int64    // Rid
	C int64    // CateId
	T string   // TagsId
	N string   // Title
	O string   // OriginUri(for debugging,can remove~!)
	B string   //	Thumbs
	P []string // RelativePath
	//D string   // CreateTime
	D int64
}

// SaveRawInfo2DB .
func (p *TagDBHelperInfo) SaveRawInfo2DB(pageTitle string, cateID int64, tagsID string, pageURI string, indexImage string, imgURI []string) (int64) {
	//log.Printf("SaveRawInfo2DB.begin...")
	//s := time.Now()

	var imgURIShort = make([]string, 0)
	for _, v := range imgURI {
		imgURIShort = append(imgURIShort, ccutility.RemoveHostName(v))
	}

	data, err := json.Marshal(imgURIShort)
	if err != nil {
		return 0
	}

	tx, _ := config.MySQL.Begin()
	defer tx.Rollback()

	var sqlStr strings.Builder
	sqlStr.WriteString("insert into ccspider_pages (Title,CateId,TagsId,OriginUri,Thumbs,RelativePath) values ('" + pageTitle + "'," + fmt.Sprintf("%v", cateID) + ",'" + tagsID + "','" + pageURI + "','" + indexImage + "','" + string(data) + "') ON DUPLICATE KEY update Title=values(Title),CateId=values(CateId),TagsId=values(TagsId),OriginUri=values(OriginUri),Thumbs=values(Thumbs),RelativePath=values(RelativePath);")

	lastID, err := tx.Insert(sqlStr.String())
	if err != nil {
		log.Printf("SaveRawInfo2DB.pageUri[%v].err[%v]", pageURI, err)
		return 0
	}

	tx.Commit()

	if lastID < 1 {
		log.Printf("SaveRawInfo2DB.pageUri[%v].lastID[%v]", pageURI, lastID)
		return 0
	}

	t := &TagRawInfo{
		lastID,
		cateID,
		tagsID,
		pageTitle,
		pageURI,
		indexImage,
		imgURIShort,
		//time.Now().Format("2006-01-02 15:04:05"),
		time.Now().In(time.FixedZone("CST", 8*3600)).Unix(),
	}

	data, err = json.Marshal(t)
	if err != nil {
		return 0
	}

	// update cache
	idx := ccutility.KeyIdx(lastID)
	config.RedisPool.Exec("HMSET", fmt.Sprintf("%v:%v", config.GlobalURIRaw, idx), lastID, data)

	//e := time.Now()
	//diff := e.Unix() - s.Unix()
	//log.Printf("SaveRawInfo2DB.end.cost[%vs]", diff)

	return lastID
}

// SaveThumbs2DB .
func (p *TagDBHelperInfo) SaveThumbs2DB(pageTitle string, pageURI string, indexImage string, imgURI []string) (int64) {
	//log.Printf("SaveThumbs2DB.begin...")
	//s := time.Now()

	var imgURIShort = make([]string, 0)
	for _, v := range imgURI {
		imgURIShort = append(imgURIShort, ccutility.RemoveHostName(v))
	}

	data, err := json.Marshal(imgURIShort)
	if err != nil {
		return 0
	}

	tx, _ := config.MySQL.Begin()
	defer tx.Rollback()

	var sqlStr strings.Builder
	sqlStr.WriteString("insert into ccspider_pages (Title,OriginUri,Thumbs,RelativePath) values ('" + pageTitle + "','" + pageURI + "','" + indexImage + "','" + string(data) + "') ON DUPLICATE KEY update Title=values(Title),OriginUri=values(OriginUri),Thumbs=values(Thumbs),RelativePath=values(RelativePath);")

	lastID, err := tx.Insert(sqlStr.String())
	if err != nil {
		log.Printf("SaveThumbs2DB.pageUri[%v].err[%v]", pageURI, err)
		return 0
	}

	tx.Commit()

	if lastID < 1 {
		log.Printf("SaveThumbs2DB.pageUri[%v].lastID[%v]", pageURI, lastID)
		return 0
	}

	// update cache
	idx := ccutility.KeyIdx(lastID)
	keyStr := fmt.Sprintf("%v:%v", config.GlobalURIRaw, idx)
	reply, err := redis.Strings(config.RedisPool.Exec("HMGET", keyStr, lastID))
	if err != nil {
		log.Printf("SaveThumbs2DB.pageUri[%v].lastID[%v].HMGET.err[%v]", pageURI, lastID, err)
		return 0
	}

	t := &TagRawInfo{}

	// rebuild hash item
	if len(reply[0]) == 0 {
		t, err = p.QueryPages(pageURI, t)
		if err != nil {
			log.Printf("SaveThumbs2DB.pageUri[%v].lastID[%v].QueryPages.err[%v]", pageURI, lastID, err)
			return 0
		}
	} else {
		if err = json.Unmarshal([]byte(reply[0]), t); err != nil {
			log.Printf("SaveThumbs2DB.pageUri[%v].lastID[%v].Unmarshal.err[%v]", pageURI, lastID, err)
			return 0
		}

		t.N = pageTitle
		t.O = pageURI
		t.B = indexImage
	}

	sort.Strings(t.P)

	data, err = json.Marshal(t)
	if err != nil {
		log.Printf("SaveThumbs2DB.pageUri[%v].lastID[%v].Marshal.err[%v]", pageURI, lastID, err)
		return 0
	}

	config.RedisPool.Exec("HMSET", keyStr, lastID, data)

	//e := time.Now()
	//diff := e.Unix() - s.Unix()
	//log.Printf("SaveRawInfo2DB.end.cost[%vs]", diff)
	return lastID
}

// QueryPages .
func (p *TagDBHelperInfo) QueryPages(pageURI string, ret *TagRawInfo) (t *TagRawInfo, err error) {
	result, err := config.MySQL.Get("SELECT Rid,CateId,TagsId,Title,OriginUri,Thumbs,RelativePath,CreateTime FROM ccspider_pages WHERE OriginUri='" + pageURI + "';")
	if err != nil {
		return nil, fmt.Errorf("QueryPages.pageURI[%v].err[%v]", pageURI, err)
	}

	ret.R = result["Rid"].(int64)
	ret.C = result["CateId"].(int64)
	ret.T = result["TagsId"].(string)
	ret.N = result["Title"].(string)
	ret.O = result["OriginUri"].(string)
	ret.B = result["Thumbs"].(string)
	ret.P = strings.Split(strings.Replace(strings.Trim(result["RelativePath"].(string), "[]"), "\"", "", -1), ",")

	loc, _ := time.LoadLocation("Local")
	ts, err := time.ParseInLocation(config.GlobalBaseTime, result["CreateTime"].(string), loc)
	if err != nil {
		return nil, fmt.Errorf("QueryPages.pageURI[%v].Parse.err[%v]", pageURI, err)
	}

	ret.D = ts.Unix()

	return ret, nil
}
