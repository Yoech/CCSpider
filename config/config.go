package config

import (
	"fmt"
	"github.com/Yoech/CCSpider/ccpool"
	"github.com/Yoech/CCSpider/myredis"
	"github.com/Yoech/CCSpider/mysql"
	"github.com/gocolly/colly/queue"
	"github.com/gocolly/redisstorage"
	"runtime"
	"sync"
)

// GlobalBaseTime .
const GlobalBaseTime = "2006-01-02 15:04:05"

// GlobalURIProxyZset .
const GlobalURIProxyZset = "CCP:P:ZS"

// GlobalProxyDefaultScore .
const GlobalProxyDefaultScore = 100

// GlobalProxyMaxScore .
const GlobalProxyMaxScore = GlobalProxyDefaultScore + 10

// GlobalWorkerNum .
var GlobalWorkerNum = runtime.NumCPU()

// RedisPool .
var RedisPool *myredis.TagRedisPoolInfo

// MySQL .
var MySQL *mysql.TagMySQLPoolInfo

// TagCacheInfo .
type TagCacheInfo struct {
	S *redisstorage.Storage
	Q *queue.Queue
}

// TagStatsInfo .
type TagStatsInfo struct {
	TotalURISucc   int
	TotalRawSucc   int
	LastUpdateTime string
}

// GStats .
var GStats = make([]TagStatsInfo, 0)

// GLock .
var GLock = new(sync.RWMutex)

/****************************************/

// Ctor .
var Ctor = func(idx int) (interface{}, error) {
	c := &TagCacheInfo{nil, nil}

	c.S = &redisstorage.Storage{
		Address:  RedisPool.Fmt.String(),
		Password: RedisPool.Pass,
		DB:       RedisPool.DB,
		Prefix:   fmt.Sprintf("%v:%v", RedisPool.Prefix, idx),
	}

	var err error
	c.Q, err = queue.New(1, c.S)
	c.S.Clear()

	return c, err
}

// Dtor .
var Dtor = func(v interface{}) error {
	c := v.(*TagCacheInfo)
	if c.S != nil && c.S.Client != nil {
		c.S.Client.Close()
	}
	return nil
}

// Ping .
var Ping = func(v interface{}) error {
	return nil
}

// StoragePool .
var StoragePool ccpool.CCPool

/****************************************/

// CacheBucketMax .
var CacheBucketMax int64 = 0xFF

// PrefixHTTP .
var PrefixHTTP = "http://"

// PrefixHTTPS .
var PrefixHTTPS = "https://"

// PrefixHTTPLen .
var PrefixHTTPLen = len(PrefixHTTP)

// PrefixHTTPSLen .
var PrefixHTTPSLen = len(PrefixHTTPS)

// GlobalURICate .
var GlobalURICate = "CCSP:C"

// GlobalURITags .
var GlobalURITags = "CCSP:T"

// GlobalURIRaw .
var GlobalURIRaw = "CCSP:R"

// GlobalErrorPage .
var GlobalErrorPage = "CCSP:E:P"

// GlobalErrorRawURI .
var GlobalErrorRawURI = "CCSP:E:R"

// GlobalErrorRawMeta .
var GlobalErrorRawMeta = "CCSP:E:M"

// GlobalGenerateDetails .
var GlobalGenerateDetails = "CCSP:G:D"

// GlobalGenerateCate .
var GlobalGenerateCate = "CCSP:G:C"

// GlobalGenerateTag .
var GlobalGenerateTag = "CCSP:G:T"

// GlobalGeneratePage .
var GlobalGeneratePage = "CCSP:G:P"

/****************************************/
