package ccpool

// CCPool .
type CCPool interface {
	Get() interface{}

	Put(interface{}) error

	Close(interface{}) error

	Dtor()

	Len() int
}
