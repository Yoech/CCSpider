package ccpool

import (
	"errors"
	"fmt"
	"sync"
	"time"
)

type channelPool struct {
	counts      int
	mu          sync.Mutex
	conns       chan *idleConn
	ctor        func(int) (interface{}, error)
	dtor        func(interface{}) error
	pingFunc    func(interface{}) error
	idleTimeout time.Duration
}

type idleConn struct {
	conn interface{}
	t    time.Time
}

// NewPool .
func NewPool(InitialCap int, MaxCap int, Factory func(int) (interface{}, error), Close func(interface{}) error, Ping func(interface{}) error, IdleTimeout time.Duration) CCPool {
	if InitialCap < 0 || MaxCap <= 0 || InitialCap > MaxCap {
		return nil
	}
	if Factory == nil {
		return nil
	}
	if Close == nil {
		return nil
	}

	p := &channelPool{
		conns:       make(chan *idleConn, MaxCap),
		ctor:        Factory,
		dtor:        Close,
		idleTimeout: IdleTimeout,
	}

	if Ping != nil {
		p.pingFunc = Ping
	}

	for i := 0; i < InitialCap; i++ {
		conn, err := p.ctor(i)
		if err != nil {
			p.Dtor()
			return nil
		}
		p.conns <- &idleConn{conn: conn, t: time.Now()}
	}

	p.counts = InitialCap

	return p
}

func (p *channelPool) getConns() chan *idleConn {
	p.mu.Lock()
	conns := p.conns
	p.mu.Unlock()
	return conns
}

func (p *channelPool) Get() interface{} {
	conns := p.getConns()
	if conns == nil {
		return nil
	}
	for {
		select {
		case wrapConn := <-conns:
			if wrapConn == nil {
				return nil
			}
			if timeout := p.idleTimeout; timeout > 0 {
				if wrapConn.t.Add(timeout).Before(time.Now()) {
					p.Close(wrapConn.conn)
					continue
				}
			}
			if err := p.Ping(wrapConn.conn); err != nil {
				fmt.Println("conn is not able to be connected: ", err)
				continue
			}
			return wrapConn.conn
		default:
			p.mu.Lock()
			if p.ctor == nil {
				p.mu.Unlock()
				continue
			}
			p.counts++
			conn, err := p.ctor(p.counts)
			p.mu.Unlock()
			if err != nil {
				return nil
			}

			return conn
		}
	}
}

func (p *channelPool) Put(conn interface{}) error {
	if conn == nil {
		return errors.New("connection is nil. rejecting")
	}

	p.mu.Lock()

	if p.conns == nil {
		p.mu.Unlock()
		return p.Close(conn)
	}

	select {
	case p.conns <- &idleConn{conn: conn, t: time.Now()}:
		p.mu.Unlock()
		return nil
	default:
		p.mu.Unlock()
		return p.Close(conn)
	}
}

func (p *channelPool) Close(conn interface{}) error {
	if conn == nil {
		return errors.New("connection is nil. rejecting")
	}
	p.mu.Lock()
	defer p.mu.Unlock()
	if p.dtor == nil {
		return nil
	}
	return p.dtor(conn)
}

func (p *channelPool) Ping(conn interface{}) error {
	if conn == nil {
		return errors.New("connection is nil. rejecting")
	}
	if p.pingFunc == nil {
		return nil
	}
	return p.pingFunc(conn)
}

func (p *channelPool) Dtor() {
	p.mu.Lock()
	conns := p.conns
	p.conns = nil
	p.ctor = nil
	p.pingFunc = nil
	closeFun := p.dtor
	p.dtor = nil
	p.mu.Unlock()

	if conns == nil {
		return
	}

	close(conns)
	for wrapConn := range conns {
		closeFun(wrapConn.conn)
	}
}

func (p *channelPool) Len() int {
	return len(p.getConns())
}
