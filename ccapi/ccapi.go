package ccapi

import (
	"encoding/json"
	"github.com/Yoech/CCSpider/ccyaml"
	"github.com/Yoech/CCSpider/config"
	"github.com/Yoech/CCSpider/logic"
	"log"
	"net/http"
	"regexp"
	"strings"
)

// TagRouteInfo .
type TagRouteInfo struct {
	expr string
	f    func(w http.ResponseWriter, r *http.Request)
}

var routePath = []TagRouteInfo{
	{"(?i)stats$", HandlerStats},
	{"(?i)search/(.*)", HandlerSearch},
}

// Run .
func Run(api ccyaml.TagYamlAPIInfo) {
	mux := http.NewServeMux()
	mux.HandleFunc("/", HandleRoute)
	log.Println("Starting server", api.Listen)
	http.ListenAndServe(api.Listen, mux)
}

// HandleRoute .
func HandleRoute(w http.ResponseWriter, r *http.Request) {
	api := ccyaml.YamlConf.API
	for _, p := range routePath {
		reg, err := regexp.Compile(api.Version + "/" + p.expr)
		if err != nil {
			continue
		}
		b := reg.FindStringSubmatch(r.URL.Path)
		if len(b) > 0 {
			p.f(w, r)
			break
		}
	}
}

// HandlerStats .
func HandlerStats(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		return
	}
	config.GLock.RLock()
	defer config.GLock.RUnlock()

	b, err := json.Marshal(config.GStats)
	if err != nil {
		return
	}
	w.Header().Set("content-type", "application/json")
	w.Write(b)
}

// HandlerSearch .
func HandlerSearch(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		return
	}
	api := ccyaml.YamlConf.API
	uri := strings.Replace(r.RequestURI, api.Version, "", -1)
	log.Printf("uri=%v", uri)

	if len(ccyaml.YamRules) == 0 {
		return
	}

	// e.g: /v1/search/?k=mzitu.com&v=/search/漂亮小姐姐诗诗kik/
	q := r.URL.Query()
	k := q.Get("k")
	v := q.Get("v")

	if len(k) == 0 || len(v) == 0 {
		return
	}

	bFound := false
	rule := &ccyaml.TagYamlRulesInfo{}
	for _, rs := range ccyaml.YamRules {
		if strings.Contains(rs.Site.URI, k) {
			bFound = true
			rule = rs
			break
		}
	}

	if !bFound {
		return
	}

	logic.Search(rule.Site.URI+v, rule, false, true)

	config.GLock.RLock()
	defer config.GLock.RUnlock()

	b, err := json.Marshal(config.GStats)
	if err != nil {
		return
	}
	w.Header().Set("content-type", "application/json")
	w.Write(b)
}
