package main

import (
	"github.com/Yoech/CCSpider/ccapi"
	"github.com/Yoech/CCSpider/ccyaml"
	"github.com/Yoech/CCSpider/config"
	"github.com/Yoech/CCSpider/dbhelper"
	"github.com/Yoech/CCSpider/logic"
	"log"
	"os"
	"strings"
)

func main() {
	// check test mode
	params := os.Args
	var bTestMode = false
	if len(params) > 1 {
		test := os.Args[1]
		if strings.Contains(test, "-test.") {
			bTestMode = true
		}
	}
	log.Printf("bTestMode[%v].params=%v", bTestMode, params)

	// load config and rules from yaml files.
	if ccyaml.LoadConf("./Runtime/Config/Conf.yaml") == true {

		dbhelper.DBHelper.ReloadCategoryAndTags()

		go func() {
			ccapi.Run(ccyaml.YamlConf.API)
		}()

		logic.Run(bTestMode)
	}

	// show cache status and close spider
	config.RedisPool.Status()

	log.Printf("Done!")
	return
}
