/*
SQLyog Ultimate v8.32
MySQL - 5.5.57 : Database - ccspider
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ccspider` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `ccspider`;

/*Table structure for table `ccspider_category` */

CREATE TABLE if not exists `ccspider_category` (
  `Rid` int(11) NOT NULL AUTO_INCREMENT,
  `CateName` varchar(32) NOT NULL DEFAULT '',
  `CateUri` varchar(255) DEFAULT '',
  `OrderNo` int(11) DEFAULT '0',
  `IsVisiable` tinyint(1) DEFAULT '1',
  `TplName` varchar(255) DEFAULT 'cate.html',
  `Title` varchar(255) DEFAULT '',
  `Keywords` varchar(255) DEFAULT '',
  `Description` varchar(255) DEFAULT '',
  PRIMARY KEY (`Rid`),
  UNIQUE KEY `CateName` (`CateName`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `ccspider_pages` */

CREATE TABLE if not exists `ccspider_pages` (
  `Rid` int(11) NOT NULL AUTO_INCREMENT,
  `CateId` int(11) NOT NULL DEFAULT '0',
  `TagsId` varchar(255) NOT NULL DEFAULT '',
  `Title` varchar(255) NOT NULL DEFAULT '',
  `OriginUri` varchar(255) NOT NULL DEFAULT '',
  `Thumbs` varchar(255) NOT NULL DEFAULT '',
  `RelativePath` text NOT NULL,
  `CreateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Rid`),
  UNIQUE KEY `Title` (`Title`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Table structure for table `ccspider_tags` */

CREATE TABLE if not exists `ccspider_tags` (
  `Rid` int(11) NOT NULL AUTO_INCREMENT,
  `TagName` varchar(32) NOT NULL DEFAULT '',
  `TagUri` varchar(255) DEFAULT '',
  `TplName` varchar(255) DEFAULT 'tag.html',
  `Title` varchar(255) DEFAULT '',
  `Keywords` varchar(255) DEFAULT '',
  `Description` varchar(255) DEFAULT '',
  PRIMARY KEY (`Rid`),
  UNIQUE KEY `TagName` (`TagName`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
